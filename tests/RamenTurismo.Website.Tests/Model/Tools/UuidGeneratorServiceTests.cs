﻿using RamenTurismo.Website.Client.Model;

namespace RamenTurismo.Website.Tests.Model.Tools;

public class UuidGeneratorServiceTests
{
    private readonly UuidGeneratorService _service;

    public UuidGeneratorServiceTests()
    {
        _service = new UuidGeneratorService();
    }

    [Theory]
    [InlineData(1)]
    [InlineData(10)]
    public void ShouldGenerate(int count)
    {
        // Act
        IEnumerable<Guid> uuids = _service.Generate(count);

        // Assert
        uuids.Should().HaveCount(count);
    }
}
